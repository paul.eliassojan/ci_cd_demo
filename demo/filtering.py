# -*- coding: utf-8 -*-
#!/bin/bash/python3

import re
import sys
import fileinput
import nltk
import string
import os
import subprocess
from nltk import word_tokenize
from ml2en import ml2en
from spellchecker import SpellChecker
from nltk.stem import PorterStemmer
spell = SpellChecker()
ps = PorterStemmer()

class filter():
    def __init__(self):
        pass

    def filtering(self):

        l = []
        flag = 0
        text = open("../data/comments.txt","r").read()
        text = self.remove_emoji(text)
        p=self.pattern(text)
        file=open('filter.txt','w')
        lines=p.splitlines()
        for line in lines:
            if (len(line)!=0):
                split=line.split()
                for word in split:
                    if word[0]!='@':
                        file.write(word+' ')
                file.write('\n')
        file.close()
        file1=open('filter.txt','r').readlines()
        file2=open('filter.txt','w')
        word_file=open("../data/words.txt","r").readlines()

        for i in file1:
            if i=='\n':
                continue
            else:
                    rec=self.remove(i)
                    #print(rec)
                    if rec==None or rec==" ":
                        continue
                    else:
                        ma_en=ml2en.transliterate(self.pattern(rec))
                        #ml_only=eng(ma_en)
                        file2.write(ma_en+"\n")
                    #file2.write('\n')
        file2.close()

    def remove_emoji(self,string):    ##remove emojis
        emoji_pattern = re.compile("["
                              u"\U0001F600-\U0001F64F"  # emoticons
                                   u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                                   u"\U0001F680-\U0001F6FF"  # transport & map symbols
                                   u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                                   u"\U00002500-\U00002BEF"  # chinese char
                                   u"\U00002702-\U000027B0"
                                   u"\U00002702-\U000027B0"
                                   u"\U000024C2-\U0001F251"
                                   u"\U0001f926-\U0001f937"
                                   u"\U00010000-\U0010ffff"
                                   u"\u2640-\u2642"
                                   u"\u2600-\u2B55"
                                   u"\u200d"
                                   u"\u23cf"
                                   u"\u23e9"
                                   u"\u231a"
                                   u"\ufe0f"  # dingbats
                                   u"\u3030"
                               "]+", flags=re.UNICODE)
        return emoji_pattern.sub(r' ', string)


    def pattern(self,str):  ## remove punctuations in the text
        punc = '''!-;:'"\,()./?#$%'''

        for ele in str:
            if ele in punc:
                str = str.replace(ele," ")
        str = re.sub(' +', ' ', str)

        return '\n'.join([i for i in str.split('\n') if len(i) > 0]) ##remove empty lines between text

    def remove(self,sent): ## remove english sentences from the dataset so their will be only manglish and malayalam
        words = set(nltk.corpus.words.words())
        word_file=open("../data/words.txt","r").readlines()
        c=0
        o=""
        s=word_tokenize(sent)
        if (s[0].isalnum()==False):
            for i in s:
                #print(i,end=' ')
                o=o+i+" "
            return o
        else:
            for w in s:
                misspelled = spell.unknown([w])
                for j in misspelled:
                    w=spell.correction(j)
                w=ps.stem(w)
                for l in word_file:
                    if l[:-1]==w.lower():
                        #print(w)
                        c=c+1
            if(c!=len(s)):
                print(sent,end=" ")
                return sent

p1=filter()
p1.filtering()
